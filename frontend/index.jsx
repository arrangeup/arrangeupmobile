import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter, Link, IndexRoute, HashRouter, withRouter} from 'react-router-dom';

import ProfileView from './Components/ProfileView.jsx';
import Home from './Components/Home.jsx'
import CustomerRequestView from './Components/CustomerRequestView.jsx';
import ProviderView from './Components/ProviderView.jsx';
import ProviderConfirmationView from './Components/ProviderConfirmationView.jsx';
import HeatMapView from './Components/HeatMapView.jsx';
import CustomerHistory from './Components/CustomerHistory.jsx';
import AvailableProviderList from './Components/AvailableProviderList.jsx'


const Root = () => (

    <HashRouter>
        <HistoryApp />
    </HashRouter>

);

class App extends React.Component {

	constructor(props) {
    super(props);


    //Global Name
    var name = "";
    if(localStorage.getItem('name') != null && localStorage.getItem('name') != ""){
    	name = localStorage.getItem('name');
    }

    //Global Identifier
    var identifier = "";
    if(localStorage.getItem('identifier') != null && localStorage.getItem('identifier') != ""){
    	identifier = localStorage.getItem('identifier');
    }else{
    	//generate a random hash
			var maximum = 999999999999;  var minimum = 1;
			identifier = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
			localStorage.setItem('identifier', identifier);
    }

    //Global Current Requests
    if(localStorage.getItem('currentRequests') != "null" && localStorage.getItem('currentRequests') != "" && localStorage.getItem('currentRequests') != null){
      var currentRequests = JSON.parse(localStorage.getItem('currentRequests'));
    } else {
      localStorage.setItem('currentRequests', JSON.stringify([]));
    }

    this.state = {
      name: name,
      identifier: identifier,
      available: false,
      location: "I'm lost!",
      latitude: null,
      longitude: null
    }

    //+++++++++++++++++++++++++++++
		//  PHONEGAP GEOLOCATION
		//+++++++++++++++++++++++++++++
		// Wait for device API libraries to load
		var that = this;

    document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    function onDeviceReady() {
    		console.log("Geolocation Start");
        navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
    }

    // onSuccess Geolocation
    function onSuccess(position) {
			console.log("Geolocation Complete");
			that.setState({
				location : position.coords.latitude + "," + position.coords.longitude,
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			});
    }

    // onError Callback receives a PositionError object
    function onError(error) {
    	//do nothing
    }


    //+++++++++++++++++++++++++++++
		//  SATORI MESSAGING
		//+++++++++++++++++++++++++++++

		//use satori messaging to publish your location
		this.RTM = require("satori-rtm-sdk");

		//client is what you use to publish messages
		this.client = new this.RTM("wss://qbtfuw7y.api.satori.com", "9606ABC4deF8ADD418bB64AEaEC3Ffac");

		//extra error handling
		this.client.on('enter-connected', function () {
			console.log('Connected to Satori RTM!');
		});
		this.client.on('error', function (error) {
			console.log('Failed to connect', error);
		});

		//start the client program
		this.client.start();

		//Publish our location every so often
    setInterval(function(){
    	if(this.state.available == true &&
    		this.state.latitude != null &&
    		this.state.longitude != null){

    		//publish the location
    		this.publishProviders();
    	}

    }.bind(this),2000);


    //Confirmation Channel
    //unsub the confirmation channel
		this.confirmationchannel = null;

  }

  setName(val){
  	this.setState({
  		name: val
  	});
  	localStorage.setItem('name', val);
  }

  setAvailability(val){
  	this.setState({
  		available: val
  	});
  }

	publishProviders(){

  	var pubobject = {
  		identifier: this.state.identifier,
  		name: this.state.name,
  		location: this.state.location,
  		latitude: this.state.latitude,
  		longitude: this.state.longitude,
  	};

  	this.client.publish('providers', pubobject, function (pdu) {
			if (!pdu.action.endsWith('/ok')) {
				console.log('something went wrong');
			}
		});
  }

	confirmationChannelStart(){
		//start the confirmation channel
    if(this.confirmationchannel == null){
  		var that = this;
  		this.confirmationchannel = this.client.subscribe('confirmation', this.RTM.SubscriptionMode.SIMPLE);
      this.confirmationchannel.on('rtm/subscription/data', function(pdu) {
  			that.confirmationChannelCheck(pdu);
      });
    }
	}

	confirmationChannelCheck(pdu){
  	console.log("checkIncomingConfirmation");
		var incomingConfirmations = pdu.body.messages.map(function (msg) {
			return {
				providerIdentifier: msg.providerIdentifier,
        providerName: msg.providerName,
				customerIdentifier: msg.customerIdentifier,
        customerName: msg.customerName,
				status: msg.status,
				distance: msg.distance
			}
		});

  	for(var i = 0; i < incomingConfirmations.length; i++){
  		if(this.state.identifier == incomingConfirmations[i].providerIdentifier){
  			this.confirmationChannelMatchProvider(incomingConfirmations[i]);
  		}

      if(this.state.identifier == incomingConfirmations[i].customerIdentifier){
        this.confirmationChannelMatchCustomer(incomingConfirmations[i]);
      }
  	}
  }

  confirmationChannelMatchCustomer(incomingConfirmation) {

    if(incomingConfirmation.status == 2){
      alert("match someone is going to help you");
    }

    if(incomingConfirmation.status == 0){
      alert("match no one is going to help you");
    }
  }

  confirmationChannelMatchProvider(incomingConfirmation){
    //case: customer inquires of provider to fulfill a job status is 1.
    if(incomingConfirmation.status == 1){
      this.props.history.push('/providerconfirmation/' + incomingConfirmation.providerIdentifier + '/' + incomingConfirmation.customerIdentifier + '/' + incomingConfirmation.distance + '/' + incomingConfirmation.status + '/' + incomingConfirmation.customerName);
    }

    if(incomingConfirmation.status == 2){
      alert("match you are going to help someone");
    }

    if(incomingConfirmation.status == 0){
      alert("match you are not going to help someone");
    }

  }

  confirmationChannelStop(){
  	//unsub the confirmation channel
		this.client.unsubscribe('confirmation');
		this.confirmationchannel = null;
  }


  render() {

    return (
      <div>
      	<div className="topbar">
        	<div style={{display: "inline-block"}}>
						<a className="topbar-menubtn">
							<img className="topbar-menubtn_logo" src="img/Menu_White.png"/>
							<img className="topbar-menubtn_logo" src="img/Logo_White.png"/>
						</a>
        	</div>
        	<div style={{display: "inline-block", marginLeft: "15px"}}>
        		<p style={{color: "white", margin: "0px", textShadow: "1px 1px #777"}}>ArrangeUp</p>
        	</div>
        </div>
        <div className="mainSection">

					<div className="mainContent">
						<Route exact path="/" component={Home} />
						<Route path="/customer" component={() => (
							<CustomerRequestView
								name={this.state.name}
								locationString={this.state.location}
								longitude={this.state.longitude}
								latitude={this.state.latitude} />
						)}/>
						<Route path="/provider" component={() => (
							<ProviderView
								name={this.state.name}
								identifier={this.state.identifier}
								location={this.state.location}
								longitude={this.state.longitude}
								latitude={this.state.latitude}
								available={this.state.available}
								setAvailability={this.setAvailability.bind(this)}
								confirmationChannelStart={this.confirmationChannelStart.bind(this)}
								confirmationChannelStop={this.confirmationChannelStop.bind(this)}/>
						)}/>
						<Route path="/providers" component={() => (
              <AvailableProviderList
                name={this.state.name}
                identifier={this.state.identifier}
                locationString={this.state.location}
                longitude={this.state.longitude}
                latitude={this.state.latitude}
                confirmationChannelStart={this.confirmationChannelStart.bind(this)}
                client={this.client}/>
            )} />
						<Route path="/profile" component={() => (
							<ProfileView
								name={this.state.name}
								setName={this.setName.bind(this)}/>
						)} />
						<Route path="/providerconfirmation/:providerIdentifier/:customerIdentifier/:distance/:status/:customerName" component={() => (
							<ProviderConfirmationView
                client={this.client}
                providerName={this.state.name}
                providerIdentifier={this.state.identifier}/>
						)} />
						<Route path="/map" component={() => (
							<HeatMapView />
						)} />
            <Route path="/customerhistory" component={() => (
              <CustomerHistory />
            )} />
					</div>

					<div className="sideBar">
						<ul>
							<li><Link to='/customer'><p>Search</p></Link></li>
							<li><Link to='/provider'><p>Post A Service</p></Link></li>
							<li><Link to='/map'><p>Map</p></Link></li>
              <li><Link to='/customerhistory'><p>Request History</p></Link></li>
							<li><Link to='/profile'><p>Profile</p></Link></li>
						</ul>
					</div>

        </div>
      </div>
    );
  }
}

var HistoryApp = withRouter(App);
document.addEventListener('DOMContentLoaded', function () {
  ReactDOM.render(<Root />, document.getElementById('root'));
});
