import React from "react";

class HeatMapView extends React.Component {

  constructor(props) {
    super(props);


	

  }
  
	componentDidMount() {
		// Connect the initMap() function within this class to the global window context,
		// so Google Maps can invoke it
		window.initMap = this.initMap;
		// Asynchronously load the Google Maps script, passing in the callback reference
		loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyBWpQmgDdKm3htrItAnJBrTmzwXgtUEwJc&libraries=visualization&callback=initMap')
	}
	
	initMap() {
		
		var heatMapDataCustomer = [
			{location: new google.maps.LatLng(37.782, -122.447), weight: 10},
			new google.maps.LatLng(37.782, -122.445),
			{location: new google.maps.LatLng(37.782, -122.443), weight: 10},
			{location: new google.maps.LatLng(37.782, -122.441), weight: 10},
			{location: new google.maps.LatLng(37.782, -122.439), weight: 10},
			new google.maps.LatLng(37.782, -122.437),
			{location: new google.maps.LatLng(37.782, -122.435), weight: 10},

			{location: new google.maps.LatLng(37.785, -122.447), weight: 10},
			{location: new google.maps.LatLng(37.785, -122.445), weight: 10},
			new google.maps.LatLng(37.785, -122.443),
		];

		var heatMapDataProvider = [
			{location: new google.maps.LatLng(37.785, -122.447), weight: 10},
			{location: new google.maps.LatLng(37.785, -122.445), weight: 10},
			new google.maps.LatLng(37.785, -122.443),
			{location: new google.maps.LatLng(37.785, -122.441), weight: 10},
			new google.maps.LatLng(37.785, -122.439),
			{location: new google.maps.LatLng(37.785, -122.437), weight: 10},
			{location: new google.maps.LatLng(37.785, -122.435), weight: 10}
		];		

		var sanFrancisco = new google.maps.LatLng(37.774546, -122.433523);
		
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: sanFrancisco
		});
		map.setMapTypeId('terrain');
		
		var heatmapCustomer = new google.maps.visualization.HeatmapLayer({
			data: heatMapDataCustomer
		});
		heatmapCustomer.setMap(map);
		var heatmapProvider = new google.maps.visualization.HeatmapLayer({
			data: heatMapDataProvider
		});
		heatmapProvider.setMap(map);
		
		
		var gradient = [
			'rgba(0, 255, 255, 0)',
			'rgba(0, 255, 255, 1)',
			'rgba(0, 191, 255, 1)',
			'rgba(0, 127, 255, 1)',
			'rgba(0, 63, 255, 1)',
			'rgba(0, 0, 255, 1)',
			'rgba(0, 0, 223, 1)',
			'rgba(0, 0, 191, 1)',
			'rgba(0, 0, 159, 1)',
			'rgba(0, 0, 127, 1)',
			'rgba(63, 0, 91, 1)',
			'rgba(127, 0, 63, 1)',
			'rgba(191, 0, 31, 1)',
			'rgba(255, 0, 0, 1)'
		]
		heatmapProvider.set('gradient', heatmapProvider.get('gradient') ? null : gradient);
		
	}
    
	render() {
		return (
			<div style={{width: "100%", height: "100%"}}>
				<div id="map" style={{width: "100%", height: "100%"}}></div>
			</div>
		);
	}

}

export default HeatMapView;


function loadJS(src) {
    var ref = window.document.getElementsByTagName("script")[0];
    var script = window.document.createElement("script");
    script.src = src;
    script.async = true;
    ref.parentNode.insertBefore(script, ref);
}
