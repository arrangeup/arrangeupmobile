import React from 'react';

class CurrentRequests extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if(localStorage.getItem('currentRequests') != "null" && localStorage.getItem('currentRequests') != "" && localStorage.getItem('currentRequests') != null){
      var currentRequests = JSON.parse(localStorage.getItem('currentRequests'));
    } else {
      var currentRequests = localStorage.setItem('currentRequests', JSON.stringify([]));
    }
    console.log(currentRequests);
    currentRequests = currentRequests.map(function(request, idx) {
      var status = ""
      if (request.status === 1) {
        status = "Pending";
      } else if (request.status === 2) {
        status = "Confirmed";
      }

      return (
				<div key={idx} style={{border:"1px solid #999", padding: "10px"}}>
					<div>
						<p className="text_1" style={{display: "inline-block", float: "left", padding: "2px 0 0 0"}}>{request.providerName}</p>
					</div>
					<div>	
						<p className="text_2" style={{display: "inline-block", float: "left", padding: "10px 0 0 20px"}}>Distance: {request.distance}</p>
					</div>
					<div>
						<p className="text_2" style={{display: "inline-block", float: "right", padding: "10px 0 0 20px"}}>Status: {status}</p>
					</div>
					<div className="clearfix"></div>
				</div>
      )
    })

    return (
      <div>
      	<div className="clearfix"></div>
      	<div style={{width: "100%", textAlign: "center", background: "#ddd", borderBottom: "1px solid #666"}}>
        	<p className="text_1" style={{marginBottom: "0px"}}>current requests</p>
        </div>
        {currentRequests}
      </div>

      );
  }
}

export default CurrentRequests;
