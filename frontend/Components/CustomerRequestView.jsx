import React from 'react';
import { withRouter } from 'react-router-dom'
import AvailableProviderList from './AvailableProviderList.jsx';
import { Route } from 'react-router-dom';


class CustomerRequestView extends React.Component {
  constructor(props) {
    super(props);

		console.log(this.props.location);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  handleSubmit(event) {
    event.preventDefault();
    this.props.history.push('/providers/' + this.props.name +"/" + this.props.latitude + "/" + this.props.longitude);
  }

  render() {
    return (
    	<div>
				<form onSubmit={this.handleSubmit}>
				
					<div style={{textAlign:'center'}}>
				
						<div style={{padding:'100px 0 0'}}>
							<input className="button_blue_large" type="submit" value="Request Service" />
						</div>
				
						<div style={{margin:'10px 0 0'}}>
							<p className="text_2">{this.props.locationString}</p>
						</div>
					</div>
      
				</form>
      </div>
    );
  }
}



export default withRouter(CustomerRequestView);
