import React from 'react';
import { withRouter } from 'react-router-dom';

class AvailableProviderItem extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();

    //publish to the confirmation channel
    var pubobject = {
      providerIdentifier: this.props.provider.identifier,
      providerName: this.props.provider.name,
      customerIdentifier: this.props.customerIdentifier,
      customerName: this.props.customerName,
      distance: this.props.provider.distance,
      status: 1
    };

    console.log(pubobject);

    this.props.client.publish('confirmation', pubobject, function (pdu) {
      if (!pdu.action.endsWith('/ok')) {
        console.log('something went wrong');
      }
    });

    this.props.confirmationChannelStart();

    if(localStorage.getItem('currentRequests') != "null" && localStorage.getItem('currentRequests') != "" && localStorage.getItem('currentRequests') != null){
      var currentRequests = JSON.parse(localStorage.getItem('currentRequests'));
    } else {
      localStorage.setItem('currentRequests', JSON.stringify([]));
    }
    var currentRequests = JSON.parse(localStorage.getItem('currentRequests'));
    currentRequests.push(pubobject);
    localStorage.setItem('currentRequests', JSON.stringify(currentRequests));

    this.props.history.push('/customerhistory');
  }

  render() {
    return (
    
    	<div style={{border:"1px solid #999", padding: "10px"}}>
    		<div>
					<p className="text_1" style={{display: "inline-block", float: "left", padding: "2px 0 0 0"}}>{this.props.provider.name}</p>
				</div>
				<div>	
					<p className="text_2" style={{display: "inline-block", float: "left", padding: "10px 0 0 20px"}}>Distance: {this.props.provider.distance}</p>
        </div>
        <div>
        	<button className="button_blue_large" style={{display: "inline-block", float: "right"}} onClick={this.handleClick}>Hire</button>
        </div>
        <div className="clearfix"></div>
      </div>
    );

  }
}

export default withRouter(AvailableProviderItem);
