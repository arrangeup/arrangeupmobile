import React from 'react';

class PastRequests extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
      	<div style={{width: "100%", textAlign: "center", background: "#ddd", borderBottom: "1px solid #666"}}>
        	<p className="text_1" style={{marginBottom: "0px"}}>past requests</p>
        </div>
        
        <div style={{border:"1px solid #999", padding: "10px"}}>
					<div>
						<p className="text_1" style={{display: "inline-block", float: "left", padding: "2px 0 0 0"}}>John Doe</p>
					</div>
					<div>
						<p className="text_2" style={{display: "inline-block", float: "right", padding: "10px 0 0 20px"}}>Status: Completed</p>
					</div>
					<div className="clearfix"></div>
				</div>
      </div>

      );
  }
}

export default PastRequests;
