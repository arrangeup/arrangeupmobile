import React from 'react';
import { Link } from 'react-router-dom';


class ProfileView extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      name: this.props.name,
    };
    
  }

	//handles the name change in the field
  handleNameChange(event) {
    this.setState({name: event.target.value});
  }
  
  handleNameSubmit(){
  	this.props.setName(this.state.name);
  }
  
  render() {
  	var correctionText;
  	
  	if(this.state.name == ""){
  		correctionText = <p className="text_2" style={{color: "red", margin: "10px 0 0"}}>please provide a profile name.</p>;
  	}
    return (
      <div style={{textAlign:'center'}}>
      
      	<div style={{padding: "50px 0px 0px", margin: "0px"}}>
      		<p className="text_1">name</p>
      	</div>
      	
      	<div>
      		<input className="input_textfield_large" style={{width:'60%'}} type="text" name="providerName" value={this.state.name} onChange={this.handleNameChange.bind(this)}/>
      	</div>
      	
      	{correctionText}
      	
      	<div style={{padding: "10px 0px 0px", margin: "0px"}}>
      		<input className="button_blue_large" type="submit" value="Submit" onClick={this.handleNameSubmit.bind(this)}/>
      	</div>
      	
      </div>
    );

  }
}

export default ProfileView;
