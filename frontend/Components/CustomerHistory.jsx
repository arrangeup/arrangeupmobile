import React from 'react'

import CurrentRequests from './CurrentRequests.jsx';
import PastRequests from './PastRequests.jsx';

class CustomerHistory extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul className="list-view">
        <CurrentRequests />
        <PastRequests />
      </ul>
    );

  }
}

export default CustomerHistory;
