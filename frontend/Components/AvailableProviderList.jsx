import React from 'react';
import AvailableProviderItem from './AvailableProviderItem.jsx';
import geolib from 'geolib';

class AvailableProviderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      providers: []
    }

    console.log(this.props.name);
  }

  componentDidMount() {
    var that = this;

    //use satori messaging to publish your location
    var RTM = require("satori-rtm-sdk");

    //client is what you use to publish messages
    this.client = new RTM("wss://qbtfuw7y.api.satori.com", "9606ABC4deF8ADD418bB64AEaEC3Ffac");

    //extra error handling
    this.client.on('enter-connected', function () {
      console.log('Connected to Satori RTM!');
    });
    this.client.on('error', function (error) {
      console.log('Failed to connect', error);
    });

    //start the client program
    this.client.start();

    //providers channel
    this.channel = this.client.subscribe('providers', RTM.SubscriptionMode.SIMPLE);

    // var customerLatitude = 37.234234;
    // var customerLongitude = -122.2348728394;

    var customerLatitude = Number(that.props.latitude);
    var customerLongitude = Number(that.props.longitude);

    //providers channel listening to new providers posting
    this.channel.on('rtm/subscription/data', function(pdu) {

      var incomingProviders = pdu.body.messages.map(function (msg) {
        var providerLatitude = Number(msg.latitude);
        var providerLongitude = Number(msg.longitude);

        var distance = geolib.getDistance(
          { latitude: customerLatitude, longitude: customerLongitude },
          { latitude: providerLatitude, longitude: providerLongitude }, 1, 1
        );

        return {
          message: msg.name + " is located at " + msg.location,
          name: msg.name,
          location: msg.location,
          distance: (Math.round(distance * 0.000621371192)) + "mi",
          identifier: msg.identifier
        }
          // that.setState({recent: msg.name + " is located at " + msg.location});
      });

      var currentProviders = that.state.providers;
      var isAdded = false;


      for (var i = 0; i < incomingProviders.length; i++) {
        for (var j = 0; j < currentProviders.length; j++) {
          if (incomingProviders[i].identifier === currentProviders[j].identifier) {
            currentProviders[j] = incomingProviders[i];
            isAdded = true
            break;
          }
        }

        if (isAdded === false) {
          currentProviders.push(incomingProviders[i]);
        }
      }
      that.setState({ providers: currentProviders });
    });
  }

  render() {
    var availableProviders = this.state.providers.map(function(provider, idx) {
      console.log(provider)
      return <AvailableProviderItem key={idx} provider={provider} customerIdentifier={this.props.identifier} client={this.props.client} customerName={this.props.name} confirmationChannelStart={this.props.confirmationChannelStart.bind(this)}/>
    }.bind(this));
    return(
      <div>
      	<div className="clearfix"></div>
      	<div style={{width: "100%", textAlign: "center", background: "#ddd", borderBottom: "1px solid #666"}}>
        	<p className="text_1" style={{marginBottom: "0px"}}>available providers</p>
        </div>
        <ul className="list-view">
          {availableProviders}
        </ul>
      </div>
    );
  }
}

export default AvailableProviderList;
