import React from "react";

class ProviderView extends React.Component {

  constructor(props) {
    super(props);



		this.confirmationchannel = null;


		//+++++++++++++++++++++++++++++
		//  MAIN
		//+++++++++++++++++++++++++++++

    this.state = {
    	live: false,
      location: "I'm lost!",
			latitude: null,
			longitude: null
    };

  }


  handleGoLive(event) {
  	event.preventDefault();
		this.props.setAvailability(true);

		//start the confirmation channel
    this.props.confirmationChannelStart();
  }

  handleStop(event) {
  	event.preventDefault();
		this.props.setAvailability(false);

		//stop the confirmation channel
		this.props.confirmationChannelStop();
  }

  //NEED TO MOVE

  render() {

  	var liveBtn;

  	if(this.props.available == false){
  		liveBtn = <input className="button_blue_large" type="submit" value="I'm Available" onClick={this.handleGoLive.bind(this)}/>;
  	}else{
  		liveBtn = <input className="button_grey_large" type="submit" value="Stop" onClick={this.handleStop.bind(this)}/>;
  	}
    return (
      <div style={{textAlign:'center'}}>

      	<div style={{padding:'100px 0 0'}}>
      		{liveBtn}
        </div>

      	<div style={{margin:'10px 0 0'}}>
        	<p className="text_2">{this.props.location}</p>
				</div>
      </div>
    );
  }
}

export default ProviderView;
