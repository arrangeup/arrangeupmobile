import React from "react";
import { withRouter } from 'react-router-dom';

class ProviderConfirmationView extends React.Component {

  constructor(props) {
    super(props);

		this.confirmationchannel = null;


		//+++++++++++++++++++++++++++++
		//  MAIN
		//+++++++++++++++++++++++++++++

    this.state = {
    	name: this.props.match.params.customerName,
    	distance: 12
    };

  }


  handleAccept(event) {
  	event.preventDefault();
    var pubobject = {
      providerIdentifier: this.props.match.params.providerIdentifier,
      providerName: this.props.match.params.providerName,
      customerIdentifier: this.props.match.params.customerIdentifier,
      providerName: this.props.match.params.customerName,
      distance: this.props.match.params.distance,
      status: 2
    };

    this.props.client.publish('confirmation', pubobject, function (pdu) {
      if (!pdu.action.endsWith('/ok')) {
        console.log('something went wrong');
      }
    });

    //TODO: Change this to provide history
    this.props.history.push('/');
  }

  handleReject(event) {
  	event.preventDefault();
    var pubobject = {
      providerIdentifier: this.props.match.params.providerIdentifier,
      providerName: this.props.match.params.providerName,
      customerIdentifier: this.props.match.params.customerIdentifier,
      providerName: this.props.match.params.customerName,
      distance: this.props.match.params.distance,
      status: 0
    };

    this.props.client.publish('confirmation', pubobject, function (pdu) {
      if (!pdu.action.endsWith('/ok')) {
        console.log('something went wrong');
      }
    });

    // TODO: Change this to provide history
    this.props.history.push('/');
  }

  render() {


    return (
      <div style={{textAlign:'center'}}>

      	<div style={{padding:'100px 0 0'}}>
      		<p className="text_1">{this.state.name}</p>
      		<p className="text_2">would like to hire you!</p>
      		<p className="text_2">distance: {this.state.distance} mi</p>
      		<div style={{display: 'inline-block', padding: '0px 10px'}}>
						<button onClick={this.handleAccept.bind(this)} className="button_blue_large" type="button">Accept</button>
					</div>
					<div style={{display: 'inline-block', padding: '0px 10px'}}>
            <button onClick={this.handleReject.bind(this)} className="button_grey_large" type="button">Reject</button>
					</div>
        </div>

      </div>
    );
  }
}

export default withRouter(ProviderConfirmationView);
